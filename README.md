Tweet Collector and Sentiment Analyser
--------------------------------------

Installing Pre-requisites
-------------------------
If Python 'setup-tools' and 'pip' are not already installed, then do the following:

    $ sudo apt-get install python-setuptools
    $ sudo easy_install pip

Then install the Elasticsearch python client, Twitter python library and NLTK python library y doing the following:

    $ pip install -U elasticsearch
    $ pip install -U tweepy
    $ pip install -U nltk


To run the Tweet Collector
--------------------------
* Set the correct Access token, Access token secret, Consumer key and Consumer secret in 'tweet.py' file. In order to get those values, go to the following URL 'https://apps.twitter.com/app/new' and create a new Twitter App. This new app will have all the 4 before mentioned keys.
* You can change the filter text to look for in a Tweet in the 'tweet.py' file. It currently looks for the texts 'java', 'javascript', 'c++' and 'python' in a tweet and only the tweets that contain one of those words, will be gathered.
* Execute the python file 'tweet.py' and re-direct its output to a file:

    $ python tweet.py > collected_tweets.txt

To run the Sentiment Analyser
------------------------------
* Set the correct Elasticearch server host IP address in 'es_loader.py' file.
* Start the Elasticsearch server instance. Running the 'es_loader.py' file, will test the Elasticsearch connection.
~~~~
    $ python es_loader.py
~~~~

* Set the tweets file that contains the tweets to be classified in 'sentiment_classifier.py'. By default, it will use the file 'res/tweet_lists.txt'
* Execute the 'sentiment_classifier.py' file.
~~~~
    $ python sentiment_classifier.py
~~~~

List of main python files:
--------------------------
tweet.py
---------
This python class can collect live Twitter Tweets and prints it to the console. The output can be redirected to a file, if one wishes to persist the tweets.

sentiment_classifier.py
-----------------------
This python class creates and trains a classifier (which can classify the sentiment of tweets as 'positive'or 'negative') by using the training corpus in 'res/training_data.csv'. Then it reads the Twitter tweets that needs to be classified from 'res/tweet_lists.txt', classifies each tweet and loads it to the Elasticsearch server, which can be visualised using Kibana.