import json
import re
from es_loader import EsLoader
from classifier import Classifier

class TweetSentimentClassifer(object):

    tweets_file_name = 'res/tweet_lists.txt'
    
    def __init__(self):
        self.__analysed_tweets = []
        self.classifier = Classifier()
        self.classifier.train()
        self.es_loader = EsLoader()

    def __get_prog_language_in_text(self, text):
        if self.__is_word_in_text('javascript', text):
            return 'javascript'
        if self.__is_word_in_text(re.escape('c++'), text):
            return 'c++'
        if self.__is_word_in_text('python', text):
            return 'python'
        return 'java'

    def __is_word_in_text(self, word, text):
        word = word.lower()
        text = text.lower()
        match = re.search(word, text)
        if match:
            return True
        return False

    def classify_tweets_in_file(self):
        tweets_file = open(self.tweets_file_name, "r")
        for line in tweets_file:
            try:
                line = line.strip()
                if line:
                    tweet = json.loads(line)
                    analysed_tweet = {'created_at' : tweet['created_at'], 'text' : tweet['text'],
                        'prog_lang' : self.__get_prog_language_in_text(tweet['text']),
			'sentiment' : self.classifier.classify(tweet['text']),
                        'user' : { 'name' : tweet['user']['name'], 'screen_name' : tweet['user']['screen_name'],
                        'followers' : tweet['user']['followers_count'],
                        'statuses_count' : tweet['user']['followers_count'],
                        'location' : tweet['user']['location']}}
		    self.es_loader.index_tweet(analysed_tweet)
		    self.__analysed_tweets.append(analysed_tweet)
            except:
                raise
    def peek_analysed_tweets(self):
        for tweet in self.__analysed_tweets[:10]:
            print tweet

if __name__ == '__main__':
    sentiment_classifier = TweetSentimentClassifer()
    sentiment_classifier.classify_tweets_in_file()
    sentiment_classifier.peek_analysed_tweets()
