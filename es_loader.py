from elasticsearch import Elasticsearch
import json

class EsLoader(object):

    mapping_text = {"lang":{"properties":{"created_at":{"type":"date","format":"EEE MMM dd HH:mm:ss Z yyyy"},"prog_lang":{"type":"string"},"sentiment":{"type":"string"},"text":{"type":"string"},"user":{"properties":{"followers":{"type":"long"},"location":{"type":"string"},"name":{"type":"string"},"screen_name":{"type":"string"},"statuses_count":{"type":"long"}}}}}}

    def __init__(self):
        self.es = Elasticsearch([{'host': '<ES_HOST>', 'port': 9200}])
        # This is the 'id' used when indexing a tweet json doc in ES
        self.i = 1
        # Delete the mapping and ignore mapping not found exception. It means no such index/type exists.
        self.es.indices.delete_mapping(index='tweets', doc_type='lang', ignore=404)
        # Create the required index
        self.es.indices.create(index='tweets', ignore=400)
        # Put the provided mapping
        self.es.indices.put_mapping(index='tweets', doc_type='lang', body=self.mapping_text)

    def test_crd(self):
        test_json = {'first_name' : 'John', 'last_name' : 'Doe'}
        self.es.index(index='testindex', doc_type='test', id=1, body=test_json)
        res = self.es.get(index='testindex', doc_type='test', id=1)
        if test_json == res['_source']:
            print "ES test success!"
        else:
            print "ES test failed!"
        self.es.delete(index='testindex', doc_type='test', id=1)

    def index_tweet(self, tweet):
        self.es.index(index='tweets', doc_type='lang', id=self.i, body=tweet)
        self.i = self.i + 1


if __name__ == '__main__':
    es_loader = EsLoader()
    es_loader.test_crd()
