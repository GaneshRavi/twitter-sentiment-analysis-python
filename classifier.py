import nltk
import csv

class Classifier(object):
    
    def read_training_data_file(self, training_data_file_name):
        """Returns a list of (tweet_text, sentiment_label)."""
        training_tweets_sentiment = []
        with open(training_data_file_name, 'rb') as f:
            reader = csv.reader(f)
            for row in reader:
                if row[0] == '0':
                    training_tweets_sentiment.append((row[1], 'negative'))
                elif row[0] == '4':
                    training_tweets_sentiment.append((row[1], 'positive'))
                else:
                    continue
        return training_tweets_sentiment

    def parse_tweet_text_as_words(self, training_tweets_sentiment):
        """ Returns a list of (tweet_words_list, sentiment).
        Any words lesser than the length of 3 are ignored.
        """
        training_tweets_words_sentiment = []
        for (words, sentiment) in training_tweets_sentiment:
            words_filtered = [e.lower() for e in words.split() if len(e) >= 3] 
            training_tweets_words_sentiment.append((words_filtered, sentiment))
        return training_tweets_words_sentiment

    def get_words_in_tweets(self, training_tweets_words_sentiment):
        """ Returns an array of all words in the provided list of
        (tweet_words_list, sentiment)
        """
        all_words = []
        for (words, sentiment) in training_tweets_words_sentiment:
            all_words.extend(words)
	return all_words

    def get_word_features(self, wordlist):
        """ Extracts a list of all the unique words from the provided word list.
        This list of all unique words is called as Word Features.
        """
        wordlist = nltk.FreqDist(wordlist)
        word_features = wordlist.keys()
        return word_features

    def extract_features(self, document):
        """ Extracts the Document Features by detecting which words of the
        document are present in the Word Features
        """
        document_words = set(document)
        features = {}
        for word in self.word_features:
            features['contains(%s)' % word] = (word in document_words)
        return features

    def train(self):
        """ Extracts the Word Features from the training dataset and uses
        it to train the classifier.
        """
        training_tweets_sentiment = self.read_training_data_file("res/training_data.csv")
        training_tweets_words_sentiment = self.parse_tweet_text_as_words(training_tweets_sentiment)
        self.word_features = self.get_word_features(self.get_words_in_tweets(training_tweets_words_sentiment))
        training_set = nltk.classify.apply_features(self.extract_features, training_tweets_words_sentiment)
        self.classifier = nltk.NaiveBayesClassifier.train(training_set)

    def classify(self, text):
        """ Classifies the given text into one of the two labels: positive,negative. """
        return self.classifier.classify(self.extract_features(text.split()))

if __name__ == '__main__':
    myclassifier = Classifier()
    myclassifier.train()
    print myclassifier.classify("I am having a bad day")
